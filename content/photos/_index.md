---
title: Photos
stylesheets: [
    "/styles/style_photos.css"
]

cascade:
  stylesheets:
    - "/styles/photos/style_photo.css"
---
