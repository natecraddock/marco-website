// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

function mouseOut() {
  if (screen.width >= 600) {
    document.getElementById("section").style.height = "40px";
  }
}
function openMenu() {
  if (document.getElementById("hambmenu").checked) {
    document.getElementById("section").style.height = "220px";
  }
  else {
    document.getElementById("section").style.height = "40px";
  }
}

// @license-end